//Start when document's ready
$(document).ready(function(){

    var cardIpt,
        cardNbr,
        cardTxt;

    //Get input card and add function when keyup
    $('#card').keyup(function(){
        //Get input card value
        cardIpt = $('#card');
        cardNbr = cardIpt.val();
        cardTxt = $('.input-group-addon');
        //Split the numbers into a array
        var arrayVal = cardNbr.split("");

        cardTxt.empty();
        checkCard(arrayVal);
    
    });

    function checkCard(array)
    {
        var lgt = array.length,
            type ;

        $('.glyphicon-ok').addClass('hidden');
        $('.glyphicon-remove').addClass('hidden');    

        switch(array[0]){
            case '4' :       
                cardTxt.append("Visa");
                type = "visa";
                if(lgt >= 13 && lgt <= 16) {
                    var valid = checkValidation(array);
                    if(!valid) {
                        cardTxt.append(" Incorrect");
                    } else {
                        $('.glyphicon-ok').removeClass('hidden');
                        $('.glyphicon-remove').addClass('hidden');
                    }
                }
                break;
            case '5' :
                cardTxt.append("MasterCard");
                type = "mastercard";
                if(lgt === 16) {
                    var valid = checkValidation(array);
                    if(!valid) {
                        cardTxt.append(" Incorrect");
                    } else {
                        $('.glyphicon-ok').removeClass('hidden');
                        $('.glyphicon-remove').addClass('hidden');
                    }
                }  
                break;
            case '3' :
                if(array[1] == '4' || array[1] == '7')
                {
                    cardTxt.append("AmericanExpress");
                    type = "americanexpress";
                }
                if(lgt === 15) {
                    var valid = checkValidation(array);
                    if(!valid) {
                        cardTxt.append(" Incorrect");
                    } else {
                        $('.glyphicon-ok').removeClass('hidden');
                        $('.glyphicon-remove').addClass('hidden');
                    }
                }     
                break;   
            default :
                cardTxt.append("?");
                break;        
        }

    }

    function checkValidation(ccArray)
    {
        var tbCc = ccArray,
            odd = -1,
            sum = 0;

        for(i=tbCc.length-1; i>=0; i--)
        {
            if(odd === 1)
            {
                var tempoNbr = tbCc[i]*2;
                if(tempoNbr > 9) tbCc[i] = parseInt(tempoNbr) - 9;
                else tbCc[i] = parseInt(tempoNbr);
            } else {
                tbCc[i] = parseInt(tbCc[i]);
            }
            odd = -odd;
        }

        for(i=0; i<tbCc.length; i++)
        {
            sum = sum+tbCc[i];
        }

        if(sum % 10 === 0) return true;
        else return false;
    }
});

