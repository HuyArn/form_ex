//Start when document's ready
$(document).ready(function(){

    var $orderVal,
        $birthVal,
        $controlVal,
        $sexSelect;


    $('.radio').click(function(){
        checkValidNumber();
    });

    $('#birth').keyup(function(){       
        checkValidNumber();
    });

    $('#order').keyup(function(){    
        checkValidNumber();        
    });

    $('#control').keyup(function(){
        checkValidNumber();
    });

    function checkValidNumber(){

        var orderCheck,
            orderLastDigit,
            birthInt,
            restControl,
            finalNbr;

        orderCheck = false;

        $sexSelect = $("input[name='sexform']:checked").val();
        $birthVal = $('#birth').val();
        $orderVal = $('#order').val();
        $controlVal = $('#control').val();

        birthNbr = $birthVal.replace(/\D/g,"");

        orderLastDigit = $orderVal%2;
        if(orderLastDigit == 0 && $sexSelect == 'F') orderCheck = true;
        if(orderLastDigit != 0 && $sexSelect == 'H') orderCheck = true;

        restControl = (birthNbr+ $orderVal)%97;
        finalNbr = 97-restControl;

        if(orderCheck && finalNbr == $controlVal) {
            $('.validation-text').empty();
            $('.validation-text').append('Correct');
            $('.validation-text').css('color','green');
        } else {
            $('.validation-text').empty();
            $('.validation-text').append('Incorrect');
            $('.validation-text').css('color','red');
        }

    }

});

